﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_down = New System.Windows.Forms.Button()
        Me.Btn_up = New System.Windows.Forms.Button()
        Me.Btn_RemiseAZero = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Lbl = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_down
        '
        Me.Btn_down.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Btn_down.Location = New System.Drawing.Point(187, 192)
        Me.Btn_down.Name = "Btn_down"
        Me.Btn_down.Size = New System.Drawing.Size(93, 47)
        Me.Btn_down.TabIndex = 0
        Me.Btn_down.Text = "-"
        Me.Btn_down.UseVisualStyleBackColor = True
        '
        'Btn_up
        '
        Me.Btn_up.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Btn_up.Location = New System.Drawing.Point(436, 192)
        Me.Btn_up.Name = "Btn_up"
        Me.Btn_up.Size = New System.Drawing.Size(91, 47)
        Me.Btn_up.TabIndex = 1
        Me.Btn_up.Text = "+"
        Me.Btn_up.UseVisualStyleBackColor = True
        '
        'Btn_RemiseAZero
        '
        Me.Btn_RemiseAZero.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Btn_RemiseAZero.Location = New System.Drawing.Point(304, 274)
        Me.Btn_RemiseAZero.Name = "Btn_RemiseAZero"
        Me.Btn_RemiseAZero.Size = New System.Drawing.Size(103, 49)
        Me.Btn_RemiseAZero.TabIndex = 2
        Me.Btn_RemiseAZero.Text = "RAZ"
        Me.Btn_RemiseAZero.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Label1.Location = New System.Drawing.Point(320, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total"
        '
        'Lbl
        '
        Me.Lbl.AutoSize = True
        Me.Lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.Lbl.Location = New System.Drawing.Point(341, 200)
        Me.Lbl.Name = "Lbl"
        Me.Lbl.Size = New System.Drawing.Size(29, 31)
        Me.Lbl.TabIndex = 4
        Me.Lbl.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Lbl)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Btn_RemiseAZero)
        Me.Controls.Add(Me.Btn_up)
        Me.Controls.Add(Me.Btn_down)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_down As Button
    Friend WithEvents Btn_up As Button
    Friend WithEvents Btn_RemiseAZero As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Lbl As Label
End Class

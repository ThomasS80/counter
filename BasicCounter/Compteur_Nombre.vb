﻿Imports BasicLib

Public Class Form1
    Dim Nombre As New Compteur_Nombre(0)
    Private Sub Btn_up_Click_1(sender As Object, e As EventArgs) Handles Btn_up.Click
        Nombre.Incrementation()
        Lbl.Text = Nombre.getNombre()
    End Sub
    Private Sub Btn_down_Click(sender As Object, e As EventArgs) Handles Btn_down.Click
        Nombre.Decrementation()
        Lbl.Text = Nombre.getNombre()
    End Sub
    Private Sub Btn_RemiseAZero_Click(sender As Object, e As EventArgs) Handles Btn_RemiseAZero.Click
        Nombre.RemiseAZero()
        Lbl.Text = Nombre.getNombre()
    End Sub
End Class

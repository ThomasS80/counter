﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicLib
{
    public class Compteur_Nombre
    {
        private int Nombre;

        public Compteur_Nombre(int Nombre)
        {
            this.Nombre = Nombre;
        }

        public void Decrementation()
        {
            this.Nombre = Nombre - 1;
            if (Nombre == -1)
            {
                this.Nombre = 0;
            }
        }
        public void Incrementation()
        {
            this.Nombre = Nombre + 1;
        }
        public void RemiseAZero()
        {
            this.Nombre = 0;
        }
        public int getNombre()
        {
            return this.Nombre;
        }
    }
}
